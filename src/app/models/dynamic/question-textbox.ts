import { QuestionBase } from './question-base';

export class TextboxQuestion extends QuestionBase<string> {
  controlType = 'textbox';
  type: string;
  placeholder: string;
  pattern: string;
  minLength: number;
  maxLength: number;

  constructor(options: {
      value?: any,
      key?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      type?: string,
      placeholder?: string,
      pattern?: string,
      minLength?: number,
      maxLength?: number} = {}) {
    super(options);
    this.type = options.type || '';
    this.placeholder = options.placeholder || '';
    this.pattern = options.pattern || '';
    this.minLength = options.minLength || null;
    this.maxLength = options.maxLength || null;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/