import { QuestionBase } from './question-base';

export class CheckboxQuestion extends QuestionBase<string> {
  controlType = 'checkbox';
  defaultChecked: boolean;
  defaultValue: boolean;
  checked: boolean;
  indeterminate: boolean;

  constructor(options: {
      value?: any,
      key?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      defaultChecked?: boolean,
      defaultValue?: boolean,
      checked?: boolean,
      indeterminate?: boolean} = {}) {
    super(options);
    this.defaultChecked = options.defaultChecked || false;
    this.defaultValue = options.defaultValue || false;
    this.checked = options.checked || false;
    this.indeterminate = options.indeterminate || false;
  }
}