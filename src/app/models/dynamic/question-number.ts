import { QuestionBase } from './question-base';

export class NumberQuestion extends QuestionBase<string> {
  controlType = 'number';
  placeholder: string;
  min: number;
  max: number;
  step: number;
  autocomplete: boolean;
  pattern: string;

  constructor(options: {
      value?: any,
      key?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      disabled?: boolean,
      placeholder?: string,
      min?: number,
      max?: number,
      step?: number,
      defaultValue?: number,
      autocomplete?: boolean,
      pattern?: string} = {}) {
    super(options);
    this.placeholder = options.placeholder || '';
    this.pattern = options.pattern || null;
    this.min = options.min || null;
    this.max = options.max || null;
    this.step = options.step || null;
    this.autocomplete = options.autocomplete || false;
  }
}