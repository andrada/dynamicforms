import { QuestionBase } from './question-base';

export class SearchBoxQuestion extends QuestionBase<string> {
  controlType = 'searchbox';
  type: string;
  listId?: string;
  options: {value: string, label?: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    this.listId = options['listId'] || null;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/