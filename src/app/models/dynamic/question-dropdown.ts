import { QuestionBase } from './question-base';

export class DropdownQuestion extends QuestionBase<string> {
  controlType = 'dropdown';
  multiple?: boolean;
  closeOnSelect?: boolean;
  searchable?: boolean;
  clearSearchOnAdd?: boolean;
  virtualScroll?: boolean;
  bindValue?: string;
  bindLabel?: string;
  options: {value: string, label: string, disabled?: boolean}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    this.multiple = options['multiple'] || null;
    this.closeOnSelect = options['closeOnSelect'] || null;
    this.searchable = options['searchable'] || null;
    this.clearSearchOnAdd = options['clearSearchOnAdd'] || null;
    this.virtualScroll = options['virtualScroll'] || null;
    this.bindValue = options['bindValue'] || null;
    this.bindLabel = options['bindLabel'] || null;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/