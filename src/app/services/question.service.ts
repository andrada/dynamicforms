import { Injectable }       from '@angular/core';

import { QuestionBase, CheckboxQuestion, DropdownQuestion, SearchBoxQuestion, TextboxQuestion, DateQuestion, RadioButtonQuestion, NumberQuestion, HtmlQuestion  }     from '../models/dynamic';


@Injectable()
export class QuestionService {

  // TODO: get from a remote source of question metadata
  // TODO: make asynchronous
  getQuestions() {

    let questions: QuestionBase<any>[] = [

      /*new DropdownQuestion({
        key: 'brave1',
        label: 'Bravery Rating',
        options: [
          {label: 'Seleccione una opcion', value: '', disabled: true},
          {label: 'Solid', value: 'solid'},
          {label: 'Great', value: 'great'},
          {label: 'Good', value: 'good'},
          {label: 'Unproven', value: 'unproven'}
        ],
        value: ['good', 'great'],
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        multiple: true,
        required: true,
        order: 2
      }),

      new DropdownQuestion({
        key: 'brave2',
        label: 'Bravery Rating',
        options: [
          {label: 'Seleccione una opcion', value: '', disabled: true},
          {label: 'Solid', value: 'solid'},
          {label: 'Great', value: 'great'},
          {label: 'Good', value: 'good'},
          {label: 'Unproven', value: 'unproven'}
        ],
        value: 'good',
        bindValue: 'value',
        bindLabel: 'label',
        searchable: true,
        clearSearchOnAdd: true,
        closeOnSelect: true,
        required: true,
        order: 2
      }),

      new SearchBoxQuestion({
        key: 'brave3',
        label: 'Bravery Rating 3',
        options: [
          {label: 'buscar', value: ''},
          {value: 'solid'},
          {value: 'great'},
          {value: 'good'},
          {value: 'unproven'}
        ],
        listId: 'test',
        required: true,
        disabled: true,
        order: 3
      }),*/

      new TextboxQuestion({
        value: 'test',
        key: 'firstName',
        label: 'First name',
        placeholder: 'Bombasto',
        required: true,
        pattern: '[a-z]#(.*)',
        minLength: 1,
        maxLength: 10,
        order: 4
      }),

      // new NumberQuestion({
      //   key: 'number',
      //   label: 'Números',
      //   required: true,
      //   order: 13,
      //   min: 6,
      //   max: 100,
      //   step: 0.5,
      //   autocomplete: true
      // })

     /* new TextboxQuestion({
        key: 'emailAddress',
        label: 'Email',
        required: true,
        type: 'email',
        order: 5
      }),

      new DateQuestion({
        value: '2010-10-12',
        key: 'date',
        label: 'Fecha',
        required: true,
        type: 'date',
        order: 4,
        min: '2000-01-06',
        max: '2030-01-01',
        autocomplete: true,
        step: 6
      }),

      new DateQuestion({
        value: '2010-10',
        key: 'month',
        label: 'Mes',
        required: true,
        type: 'month',
        order: 7,
        min: '2000-01',
        max: '2030-01',
      }),

      new DateQuestion({
        value: '2010-W06',
        key: 'week',
        label: 'Semana',
        required: true,
        type: 'week',
        order: 8,
        min: '2000-W01',
        max: '2030-W01',
      }),

      new DateQuestion({
        value: '2018-06-12T19:30',
        key: 'datetime-local',
        label: 'Datetime local',
        required: true,
        type: 'datetime-local',
        order: 9,
        min: '2017-06-07T00:00',
        max: '2020-06-14T00:00',
      }),

       new DateQuestion({
        value: '19:30',
        key: 'time',
        label: 'Hora',
        required: true,
        type: 'time',
        order: 10
      }),

      new RadioButtonQuestion({
        key: 'brave',
        label: 'Bravery Rating',
        required: true,
        options: [
          {key: 'solid',  value: 'Solid', disabled: true},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        disabled: false,
        order: 11
      }),

      new CheckboxQuestion({
        key: 'checkbox',
        label: 'Checkbox',
        required: true,
        order: 12,
        checked: true,
        indeterminate: false
      }),

      new NumberQuestion({
        key: 'number',
        label: 'Números',
        required: true,
        order: 13,
        min: 6,
        max: 100,
        step: 0.5,
        autocomplete: true,
       // pattern: '[1-9][0-9]*',
      }),

      new NumberQuestion({
        key: 'patternNumber',
        label: 'Números con regex',
        required: true,
        order: 14,
        pattern: '[1-9][0-9]*',
      }),

      new HtmlQuestion({
        key: 'html',
        order: 15,
        content: "<h1>My First Heading</h1><p>My first paragraph.</p><hr>"
      })*/
    ];

    return questions.sort((a, b) => a.order - b.order);
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/