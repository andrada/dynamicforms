import { Component, Input } from '@angular/core';
import { FormGroup, ValidationErrors }        from '@angular/forms';

import { QuestionBase }     from '../../models/dynamic/question-base';
import { TranslateService } from '@ngx-translate/core';
import { isBoolean } from 'util';

import { regularExp } from 'src/app/constants/constants';


@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html'
})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;

  constructor(private translate: TranslateService) {}

  get isValid() { return this.form.controls[this.question.key].valid; }

  getErrorMessage(): string {
    const controlErrors: ValidationErrors = this.form.controls[this.question.key].errors;
    let errorMessage: string;
    if (controlErrors != null) {
          const lastError = Object.entries(controlErrors).pop();
            if (!isBoolean(lastError[1])) {
              errorMessage = this.setErrorMessage(lastError[0], lastError[1]);
            } else {
              errorMessage = this.setErrorMessage(lastError[0]);
            }
          return errorMessage; 
        }
  }

  setErrorMessage(key: string, value?: any): string {
    let param = {param: this.getPropertyByRegex(value, "required")};
    if (key === "pattern") {
      key = this.patternMatchExpression(key, this.getPropertyByRegex(value, "required"));
    }
    let errorMessage: string;
    this.translate.get('validationErrorsMessage.' + key, param)
      .subscribe((res: string) => errorMessage = res);
     return errorMessage; 
  }

  getPropertyByRegex(obj, propName) {
    let re = new RegExp("^" + propName + "(.*)$"), key: string;
    for (key in obj) {
      if (re.test(key)) {
        return obj[key];
      }
    }
    return null; // put your default "not found" return value here
 }

  patternMatchExpression(key: string, str: string) {
    let expMatch = {
      containsNumber: regularExp.containsNumber.test(str),
      smallLetters: regularExp.containsSmallLetters.test(str),
      capitalLetters: regularExp.containsCapitalLetters.test(str),
      expecialCharacter: regularExp.containsExpecialCharacter.test(str)
    };
    let patternType = key + ".";

    if (expMatch.containsNumber && expMatch.smallLetters && expMatch.capitalLetters) {
      patternType += "alphaNumeric"; 
    }  else if(expMatch.smallLetters) {
      patternType += "smallLetters"; 
    } else if(expMatch.capitalLetters) {
      patternType += "capitalLetters"; 
    } else if (expMatch.containsNumber) {
      patternType += "numeric"; 
    } else if (expMatch.smallLetters && expMatch.capitalLetters) {
      patternType += "alphabet"; 
    } else {
      patternType += "expecialCharacter"; 
    }

    return patternType;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/