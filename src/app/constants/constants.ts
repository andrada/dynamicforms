export const regularExp = {
    containsNumber : /\d+/,
    containsExpecialCharacter : /\W+/,
    containsSmallLetters : /[a-z]/,
    containsCapitalLetters : /[A-Z]/
}